from django.urls import reverse
from django.db import models


class Pessoa(models.Model):
    nome = models.CharField(
        max_length=150,
        null=False,
        blank=False
    )

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('biblioteca:list_pessoa')


class Livro(models.Model):
    nome = models.CharField(
        max_length=150,
        null=False,
        blank=False
    )

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('biblioteca:list_livro')


class Emprestimo(models.Model):
    livro = models.ForeignKey(
        Livro,
        unique=True,
        on_delete=models.CASCADE
    )
    pessoa = models.ForeignKey(
        Pessoa,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} - {}".format(self.livro.nome, self.pessoa.nome)

    def get_absolute_url(self):
        return reverse('biblioteca:list_emprestimo')
