from django.urls import reverse
from django.views.generic import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from biblioteca.models import Livro, Pessoa, Emprestimo


class CreateLivro(CreateView):
    model = Livro
    fields = ['nome']
    template_name = 'biblioteca/form_create.html'


class UpdateLivro(UpdateView):
    model = Livro
    fields = ['nome']
    template_name = 'biblioteca/form_update.html'


class DeleteLivro(DeleteView):
    model = Livro
    template_name = 'biblioteca/form_delete.html'

    def get_success_url(self):
        return reverse('biblioteca:list_livro')


class ListLivro(ListView):
    model = Livro


class CreatePessoa(CreateView):
    model = Pessoa
    fields = ['nome']
    template_name = 'biblioteca/form_create.html'


class UpdatePessoa(UpdateView):
    model = Pessoa
    fields = ['nome']
    template_name = 'biblioteca/form_update.html'


class DeletePessoa(DeleteView):
    model = Pessoa
    template_name = 'biblioteca/form_delete.html'

    def get_success_url(self):
        return reverse('biblioteca:list_pessoa')


class ListPessoa(ListView):
    model = Pessoa


class CreateEmprestimo(CreateView):
    model = Emprestimo
    fields = ['livro', 'pessoa']
    template_name = 'biblioteca/form_create.html'


class UpdateEmprestimo(UpdateView):
    model = Emprestimo
    fields = ['livro', 'pessoa']
    template_name = 'biblioteca/form_update.html'


class DeleteEmprestimo(DeleteView):
    model = Emprestimo
    template_name = 'biblioteca/form_delete.html'

    def get_success_url(self):
        return reverse('biblioteca:list_emprestimo')


class ListEmprestimo(ListView):
    model = Emprestimo
