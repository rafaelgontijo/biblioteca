from django.conf.urls import url
from django.views.generic.base import TemplateView
from biblioteca.views import (CreateLivro, UpdateLivro, DeleteLivro,
                              ListLivro, CreatePessoa, UpdatePessoa,
                              DeletePessoa, ListPessoa, CreateEmprestimo,
                              UpdateEmprestimo, DeleteEmprestimo,
                              ListEmprestimo)

app_name = "biblioteca"

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='biblioteca/home.html'),
        name='list'),
    url(r'livro/$', ListLivro.as_view(),
        name='list_livro'),
    url(r'livro/create/$', CreateLivro.as_view(),
        name='create_livro'),
    url(r'livro/(?P<pk>\d+)/$', UpdateLivro.as_view(),
        name='update_livro'),
    url(r'livro/delete/(?P<pk>\d+)/$', DeleteLivro.as_view(),
        name='delete_livro'),
    url(r'pessoa/$', ListPessoa.as_view(),
        name='list_pessoa'),
    url(r'pessoa/create/$', CreatePessoa.as_view(),
        name='create_pessoa'),
    url(r'pessoa/(?P<pk>\d+)/$', UpdatePessoa.as_view(),
        name='update_pessoa'),
    url(r'pessoa/delete/(?P<pk>\d+)/$', DeletePessoa.as_view(),
        name='delete_pessoa'),
    url(r'emprestimo/$', ListEmprestimo.as_view(),
        name='list_emprestimo'),
    url(r'emprestimo/create/$', CreateEmprestimo.as_view(),
        name='create_emprestimo'),
    url(r'emprestimo/(?P<pk>\d+)/$',
        UpdateEmprestimo.as_view(),
        name='update_emprestimo'),
    url(r'emprestimo/delete/(?P<pk>\d+)/$',
        DeleteEmprestimo.as_view(),
        name='delete_emprestimo'),
]
